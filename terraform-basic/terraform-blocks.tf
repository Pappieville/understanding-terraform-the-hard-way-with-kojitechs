
#Terraform settings Block
terraform {
    required_version = ">=1.1.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

#Provider Block
provider "aws" {
    region = "us-east-1"
  profile = "default"
}

#Create a VPC
resource "aws_vpc" "main" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "main"
  }
}

#create EC2
resource "aws_instance" "ec2_instance" {
  ami           = var.ami_id
  instance_type = "t2.micro"

  tags = {
    Name = "ec2_instance"
  }
}